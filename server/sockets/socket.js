//Importamos el modulo IO desde el server para tener acceso a el
const { io } = require('../server');

io.on('connection', (client) => {
	console.log('Usuario conectado');

	client.emit('enviarMensaje', {
		usuario: 'Administrador',
		mensaje: 'Bienvenido a esta aplicación',
	});

	client.on('disconnect', () => {
		console.log('El usuario se desconecto');
	});

	//Escruchar el cliente
	client.on('enviarMensaje', (data, callback) => {
		console.log(data);

		// .bradcast es para emitir el mensaje a todos los usuarios conectados
		client.broadcast.emit('enviarMensaje', data);

		/* if (mensaje.usuario) {
			callback({
				response: 'Todo salio bien',
			});
		} else {
			callback({ response: 'Upps algo salio mal...' });
		} */
	});
});
