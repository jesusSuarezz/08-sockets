//Definimos las funciones que queremos que se disparen cuando recibimos o enviamos inf al server

let socket = io();

socket.on('connect', () => {
	console.log('conectado al server');
});

//Los ON son para escuchar informacion(camios)
socket.on('disconnect', () => {
	console.log('Perdimos la conexion con el servidor');
});

//Los emits son para enviar informacion
socket.emit(
	'enviarMensaje',
	{
		usuario: 'Jesus',
		mensaje: 'Hola mundo con sockets',
	},
	function (response) {
		console.log('Respuesta del servidor', response);
	}
);

//Escuchar infomracion
socket.on('enviarMensaje', function (mensaje) {
	console.log('Servidor: ', mensaje);
});
